var gulp        = require('gulp'),
    less        = require('gulp-less'),
    browserSync = require('browser-sync'),
    uglify      = require('gulp-uglifyjs'),
    del         = require('del'),
    imagemin    = require('gulp-imagemin'),
    pngquant    = require('imagemin-pngquant'),
    cache       = require('gulp-cache'),
    gutil       = require('gutil'),
    plumber     = require('gulp-plumber');


gulp.task('less', function () {
    return gulp.src('app/less/framework7.less')
        // .pipe(plumber())
        .pipe(less()
            .on('error', function(err){
                gutil.log(err);
                this.emit('end');
            })
        )
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('watch', ['browser-sync', 'less'], function() {
    gulp.watch('app/**/**/*.less', ['less']);
    gulp.watch('app/**/**/*.*', browserSync.reload);
    // gulp.watch('app/**/**/*.html', browserSync.reload);
    // gulp.watch('app/**/**/*.js', browserSync.reload);
    // gulp.watch('app/**/**/*.png', browserSync.reload);
    // gulp.watch('app/**/**/*.svg', browserSync.reload);
    // gulp.watch('app/**/**/*.jpeg', browserSync.reload);
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    });
});

gulp.task('scripts', function() {
    return gulp.src([
        'app/js/framework7.min.js',
        'app/js/jquery-3.2.1.min.js',
        'app/js/jquery.scrollTo.min.js',
        'app/js/routes.js',
        'app/js/my-app.js'
    ])
        // .pipe(uglify())
        .pipe(gulp.dest('app/js'));
});

gulp.task('clean', function() {
    return del.sync('dist');
});

gulp.task('img', function() {
    return gulp.src('app/images/**/*') // Берем все изображения из app
        .pipe(cache(imagemin({ // Сжимаем их с наилучшими настройками
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/images')); // Выгружаем на продакшен
});

gulp.task('build', ['clean', 'img', 'less', 'scripts'], function() {

    // var buildComponents = gulp.src('app/components/**/*')
    //     .pipe(gulp.dest('dist/components'));

    var buildCss = gulp.src([
        'app/css/**/*'
    ])
    .pipe(gulp.dest('dist/css'));

    var buildFonts = gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'));

    // var buildImages = gulp.src('app/images/**/*')
    // .pipe(gulp.dest('dist/images'));

    var buildJs = gulp.src('app/js/**/*')
        .pipe(gulp.dest('dist/js'));

    // var buildLess = gulp.src('app/less/**/*')
    //     .pipe(gulp.dest('dist/less'));

    // var buildModules = gulp.src('app/modules/**/*')
    //     .pipe(gulp.dest('dist/modules'));

    var buildPages = gulp.src('app/pages/**/*')
        .pipe(gulp.dest('dist/pages'));

    // var buildUtils = gulp.src('app/utils/**/*')
    //     .pipe(gulp.dest('dist/utils'));

    var buildHtml = gulp.src('app/*.html')
        .pipe(gulp.dest('dist'));

});

gulp.task('default', ['watch']);

