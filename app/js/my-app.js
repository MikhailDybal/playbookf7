// Dom7
var $$ = Dom7;

//// Theme
// var theme = 'auto';
// if (document.location.search.indexOf('theme=') >= 0) {
//     theme = document.location.search.split('theme=')[1].split('&')[0];
// }

// Init App
var app = new Framework7({
    id: 'com.PlayBook.layout',
    root: '#app',
    theme: 'md',
    routes: routes,
});


var mainView = app.views.create('.view-main');

var view = app.views.create('.view-main', {
    on: {
        pageInit: function () {
            console.log('page init')
        }
    }
});

function scroll_end_of_chat () {
    $('.page-content').scrollTo('.last-unread:eq(0)', 600, {offset:-100});
}

// function scroll_end_of_page () {
//     $$('.page-content').scrollTo({top: '100%', left: 0}, 600);
// }
//
// function scroll_top_of_page () {
//     $$('.page-content').scrollTo({top: 0, left: 0}, 600 );
// }

$$(document).on('page:init', '.page[data-name="chat"]', function (e) {


    // Init Messages
    var messages = app.messages.create({
        el: '.messages',

        // First message rule
        firstMessageRule: function (message, previousMessage, nextMessage) {
            // Skip if title
            if (message.isTitle) return false;
            /* if:
              - there is no previous message
              - or previous message type (send/received) is different
              - or previous message sender name is different
            */
            if (!previousMessage || previousMessage.type !== message.type || previousMessage.name !== message.name) return true;
            return false;
        },
        // Last message rule
        lastMessageRule: function (message, previousMessage, nextMessage) {
            // Skip if title
            if (message.isTitle) return false;
            /* if:
              - there is no next message
              - or next message type (send/received) is different
              - or next message sender name is different
            */
            if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
            return false;
        },
        // Last message rule
        tailMessageRule: function (message, previousMessage, nextMessage) {
            // Skip if title
            if (message.isTitle) return false;
            /* if (bascially same as lastMessageRule):
            - there is no next message
            - or next message type (send/received) is different
            - or next message sender name is different
          */
            if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
            return false;
        }
    });

    // Init Messagebar
    var messagebar = app.messagebar.create({
        el: '.messagebar'
    });

    // Response flag
    var responseInProgress = false;

    // Send Message
    $$('.send-link').on('click', function () {
        var text = messagebar.getValue().replace(/\n/g, '<br>').trim();
        // return if empty message
        if (!text.length) return;

        // Clear area
        messagebar.clear();

        // Return focus to area
        messagebar.focus();

        // Add message to messages
        messages.addMessage({
            text: text,
        });

        if (responseInProgress) return;
        // Receive dummy message
        receiveMessage();
    });

    // Dummy response
    var answers = [
        'Yes!',
        'No',
        'Hm...',
        'I am not sure',
        'And what about you?',
        'May be ;)',
        'Lorem ipsum dolor sit amet, consectetur',
        'What?',
        'Are you sure?',
        'Of course',
        'Need to think about it',
        'Amazing!!!'
    ];
    var people = [
        {
            name: 'Kate Johnson',
        },
        {
            name: 'Blue Ninja',
        }
    ];
    function receiveMessage() {
        responseInProgress = true;
        setTimeout(function () {
            // Get random answer and random person
            var answer = answers[Math.floor(Math.random() * answers.length)];
            var person = people[Math.floor(Math.random() * people.length)];

            // Show typing indicator
            messages.showTyping({
                header: person.name + ' is typing'
            });

            setTimeout(function () {
                // Add received dummy message
                messages.addMessage({
                    text: answer,
                    type: 'received',
                    name: person.name
                });
                // Hide typing indicator
                messages.hideTyping();
                responseInProgress = false;
            }, 4000);
        }, 1000);
    };

    // $('.size-toggle').click( function () {
    //     scroll_end_of_chat();
    // });

    $('.btn-2bottom').click( function () {
        scroll_end_of_chat();
    });

    $('.messages-content').scroll(function()
    {
        if  ($(this).scrollTop() > ($('.messages').height() - $(this).height() - 100))
        {
            $('.btn-2bottom').fadeOut('normal');
        } else {
            $('.btn-2bottom').fadeIn('normal');
        };
    });

    $$('.toolbar-inner textarea').on('click', function () {
        $$('.send-message').removeClass('hidden');
        $$('.microphone').addClass('hidden');
    });

    $$('.messages').on('click', function () {
        if (!$$('.toolbar').hasClass('record-on-proccess')) {
            $$('.send-message').addClass('hidden');
            $$('.microphone').removeClass('hidden');
        }
    });

    $$('.microphone').on('click', function (e) {
        e.preventDefault();
        $$('.toolbar').addClass('record-on-proccess');
        $$(this).addClass('hidden');
        $$('.size-toggle').addClass('hidden');
        $$('.toolbar-inner textarea').addClass('hidden');
        $$('.messagebar').addClass('messagebar-smallest-size');
        $$('.messages').addClass('messages-fullest-size');
        $$('.messagebar-progress-bar').removeClass('hidden');
        $$('.recording').removeClass('hidden');
        $$('.messagebar-timebar-wrapper').removeClass('hidden');
        $$('.stop').removeClass('hidden');
        $$('.cancel').removeClass('hidden');
        $$('.chat-fix').addClass('hidden');
        $$('.chat-decline').addClass('hidden');
    });

    $$('.cancel').on('click', function (e) {
        e.preventDefault();
        $$('.toolbar').removeClass('record-on-proccess');
        $$(this).addClass('hidden');
        $$('.microphone').removeClass('hidden');
        $$('.size-toggle').removeClass('hidden');
        $$('.toolbar-inner textarea').removeClass('hidden');
        $$('.messagebar').removeClass('messagebar-smallest-size');
        $$('.messages').removeClass('messages-fullest-size');
        $$('.messagebar-progress-bar').addClass('hidden');
        $$('.recording').addClass('hidden');
        $$('.messagebar-timebar-wrapper').addClass('hidden');
        $$('.stop').addClass('hidden');
        $$('.cancel').addClass('hidden');
        $$('.chat-fix').removeClass('hidden');
        $$('.chat-decline').removeClass('hidden');
    });

    $$('.send').on('click', function (e) {
        e.preventDefault();
        $$('.toolbar').removeClass('record-on-proccess');
        $$(this).addClass('hidden');
        $$('.microphone').removeClass('hidden');
        $$('.size-toggle').removeClass('hidden');
        $$('.toolbar-inner textarea').removeClass('hidden');
        $$('.messagebar').removeClass('messagebar-smallest-size');
        $$('.messages').removeClass('messages-fullest-size');
        $$('.messagebar-progress-bar').addClass('hidden');
        $$('.recording').addClass('hidden');
        $$('.messagebar-timebar-wrapper').addClass('hidden');
        $$('.stop').addClass('hidden');
        $$('.cancel').addClass('hidden');
        $$('.play').addClass('hidden');
        $$('.delete').addClass('hidden');
        $$('.send').addClass('hidden');
        $$('.chat-fix').removeClass('hidden');
        $$('.chat-decline').removeClass('hidden');
        $$('.messagebar-timebar').removeClass('moved-left');
    });

    $$('.stop').on('click', function (e) {
        e.preventDefault();
        $$('.toolbar').addClass('record-on-proccess');
        $$(this).addClass('hidden');
        $$('.stop').addClass('hidden');
        $$('.cancel').addClass('hidden');
        $$('.delete').removeClass('hidden');
        $$('.play').removeClass('hidden');
        $$('.send').removeClass('hidden');
        $$('.messagebar-progress-bar').removeClass('hidden');
        $$('.recording').addClass('active');
        $$('.messagebar-timebar-wrapper').removeClass('hidden');
        $$('.messagebar-timebar').addClass('moved-left');
        $$('.chat-fix').addClass('hidden');
        $$('.chat-decline').addClass('hidden');
    });


    //popup for unregistered users
    $$('.not-registered').on('click', function () {
        /* myApp.modal({
             text: '<p>Чтобы задать свой вопрос Имаму, необходимо войти в систему.</p>'+
             '<div class="button-modal-wrapper"><a href="authorization.html"><span class="modal-button modal-button-bold">Авторизоваться</span></a></div>'
         });*/

        myApp.modal({
            text: '<p>Чтобы задать свой вопрос Имаму, необходимо войти в систему.</p>',
            buttons: [
                {
                    text: 'OK',
                    onClick: function() {

                    }
                },
                {
                    text: '<a href="authorization.html">Войти</a>',
                    onClick: function() {

                    }
                }
            ]
        });

        var timeout = setTimeout(function () {
            myApp.closeModal('.modal');
        }, 5000);

        $$('.modal-overlay').on('click', function () {
            clearTimeout(timeout);
            myApp.closeModal('.modal');
        });

        $$('.modal').on('click', function () {
            clearTimeout(timeout);
            myApp.closeModal('.modal');
        });

    });

    $$('.message-audio-button').on('click', function () {
        if($$(this).hasClass('message-audio-play')) {
            $$(this).addClass('hidden');
            $$(this).parent().find('.message-audio-pause').removeClass('hidden');
            $$(this).parent().find('.round-icon-btn-loader').addClass('active');
        } else {
            $$(this).addClass('hidden');
            $$(this).parent().find('.message-audio-play').removeClass('hidden');
            $$(this).parent().find('.round-icon-btn-loader').removeClass('active');
        }
    });

});

$$(document).on('page:init', '.page[data-name="club-list"]', function (e) {

    $$('.js-popover-close').on('click', function (e) {
        e.preventDefault();
        app.popover.close('.popover-sort-by', true);
    });

});

$$(document).on('page:init', '.page[data-name="club-item"]', function (e) {

    $$('.js-modal-info').on('click', function () {
        var dialog = app.dialog.create({
            text: '<div class="slide-icon-descr-image">' + '<img src="../images/icon-' + $$(this).find('.swiper-slide-icon-image').attr('data-icon') + '-inv.png">' + '</div>' + '<p>' + $$(this).find('.swiper-slide-icon-item').attr('title')  + '</p> ' +
            '<a class="icon-close icon-close-top js-modal-close" onclick="app.dialog.close()"><i class="material-icons">clear</i></a>',
            cssClass: 'theme-dark',
        }).open();

        var timeout = setTimeout(function () {
            app.dialog.close()
        }, 3000);

        $$('.dialog-backdrop').on('click', function () {
            clearTimeout(timeout);
            app.dialog.close()
        });

    });

});

$$(document).on('page:init', '.page[data-name="event-list"]', function (e) {

    var searchbar = app.searchbar.create({
        'el': '.js-searchbar-init'
    });

    $$('.searchbar-enable').on('click', function () {
        searchbar.enable();
    });
    $$('.searchbar-close-button').on('click', function () {
        searchbar.disable();
    });

    //temp
    // searchbar.enable();

    //temp

    $$('.sheet-datepicker').on('sheet:open', function () {
        var today = new Date();
        var datepicker = app.picker.create({
            containerEl: '#picker-date-container',
            inputEl: '#picker-date-item',
            toolbar: false,
            rotateEffect: true,
            value: [
                today.getDate(),
                today.getMonth(),
                today.getFullYear(),
            ],
            formatValue: function (values, displayValues) {
                var day_cut = values[0];
                day_cut = (day_cut < 10) ? '0' + String(day_cut) : day_cut;
                var month_cut = values[1];
                month_cut = (month_cut < 10) ? '0' + String(month_cut) : month_cut;
                var year_cut = String(values[2]).substr(2);
                return day_cut + '.' + month_cut + '.' + year_cut;
            },
            cols: [
                // Days
                {
                    values: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                },
                // Months
                {
                    values: [1,2,3,4,5,6,7,8,9,10,11,12],
                    displayValues: ('Студзень Люты Сакавiк Красавiк Май Червень Лiпень Жнiвень Верасень Кастрычнiк Лiстапад Снежань').split(' '),
                    textAlign: 'left'
                },
                // // Years
                {
                    values: (function () {
                        var arr = [];
                        for (var i = 1950; i <= 2030; i++) { arr.push(i); }
                        return arr;
                    })(),
                },
            ],
            on: {
                change: function (picker, values, displayValues) {
                    var daysInMonth = new Date(picker.value[2], picker.value[0]*1 + 1, 0).getDate();
                    if (values[1] > daysInMonth) {
                        picker.cols[1].setValue(daysInMonth);
                    }
                },
            }
        });

        $$('#picker-date-item').on('change', function () {
            var datepicker_text = $$(this).val();
            $$('#picker-date-label').text(datepicker_text);
        });
    });


    // Calender
    var calendarModal = app.calendar.create({
        inputEl: '#sheet-calendar',
        openIn: 'customModal',
        header: true,
        footer: true,
        dateFormat: 'dd.MM.yyyy',
        // monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'Август' , 'September' , 'October', 'November', 'December'],
        monthNames: ['01', '02', '03', '04', '05', '06', '07', '08' , '09' , '10', '11', '12'],
        // dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чет', 'Пят', 'Суб'],

        on: {
            closed: function () {
                var calendarModalDate = new Date(calendarModal.value);
                // calendarModalDate = calendarModalDate.getDay() + '.' + calendarModalDate.getMonth() + '.' + calendarModalDate.getFullYear()
                // console.log(calendarModalDate);
                var day_cut = calendarModalDate.getDate();
                day_cut = (day_cut < 10) ? '0' + String(day_cut) : day_cut;
                var month_cut = calendarModalDate.getMonth() + 1;
                month_cut = (month_cut < 10) ? '0' + String(month_cut) : month_cut;
                var year_cut = String(calendarModalDate.getFullYear()).substr(2);
                var calendat_text = day_cut + '.' + month_cut + '.' + year_cut;
                $$('#sheet-calendar-label').text(calendat_text);

            }
        }
    });



});

$$(document).on('page:init', '.page[data-name="club-item-rating"]', function (e) {

    $$('.js-player-rating .player-rating-star').on('click', function () {
        $$(this).siblings('').removeClass('active');
        $$(this).addClass('active')
            .prevAll('').addClass('active');
    });

});

$$(document).on('page:init', '.page[data-name="enter"]', function (e) {

    var mySwiper = new Swiper('.js-swiper-bg-init', {
        speed: 2000,
        loop: true,
        autoplay: {
            delay: 8000,
        },
        effect: 'fade',
        followFinger: false,
        allowTouchMove: false,
    });
    mySwiper.init();

});

$$(document).on('page:init', '.page[data-name="invitation"]', function (e) {

    var calendarModal = app.calendar.create({
        inputEl: '#demo-calendar-modal',
        openIn: 'customModal',
        header: true,
        footer: true,
        dateFormat: 'dd MM yyyy',
        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'Август' , 'September' , 'October', 'November', 'December'],
        dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чет', 'Пят', 'Суб']
    });

    var popup_about = app.popup.create({
        'el': '.popup-about',
    });

    $$('.inv-day-list').each(function () {
        var inv_day_item = $(this).find('.inv-day-item');
        console.info(inv_day_item);
        inv_day_item.each(function (ind) {
            var inv_day_this = $(this);
            inv_day_this.on('click', function()
            {
                inv_day_item.not(inv_day_this).removeClass('active');
                inv_day_this.addClass('active');
            });
        });
    });

    //temp
    // popup_about.open();

    // $$('.popup-about');

});

$$(document).on('page:init', '.page[data-name="registration-2"]', function (e) {

    // $$('.switch6-light span').on('click', function () {
    //     $$(this).parent('span').toggleClass('ac');
    // });

    $$('.js-open-alert').on('click', function () {
        var dialog = app.dialog.create({
            title: 'Еще немного',
            text: 'Пожалуйста выберите ваш вид спорта, указав его уровень. ' +
            '<a class="icon-close icon-close-top js-modal-close" onclick="app.dialog.close()"><i class="material-icons">clear</i></a>',
            buttons: [
                {
                    text: 'Послушать течно,',
                },
                {
                    text: 'Я люблю под диск-жокеев побалдеть...',
                },
                {
                    text: 'Да, конечно!',
                },
            ],
            verticalButtons: true,
        }).open();
    });

    $$('.dialog-backdrop').on('click', function () {
        clearTimeout(timeout);
        app.dialog.close()
    });

    var timeout = setTimeout(function () {
        app.dialog.close()
    }, 3000);

    var cities = ['Москва', 'Санкт-Петербург', 'Новосибирск', 'Екатеринбург', 'Нижний Новгород', 'Казань', 'Челябинск', 'Омск', 'Самара', 'Ростов-на-Дону', 'Уфа', 'Красноярск', 'Пермь', 'Воронеж', 'Волгоград', 'Краснодар', 'Саратов', 'Тюмень', 'Тольятти', 'Ижевск', 'Барнаул', 'Ульяновск', 'Иркутск', 'Хабаровск', 'Ярославль', 'Владивосток', 'Махачкала', 'Томск', 'Оренбург', 'Кемерово', 'Новокузнецк', 'Рязань', 'Астрахань', 'Набережные Челны', 'Пенза', 'Липецк', 'Киров', 'Чебоксары', 'Тула', 'Калининград', 'Балашиха', 'Курск'];

    // AUTOCOMPLETE GO2OTHER VIEW
    // var autocompleteStandaloneSimple = app.autocomplete.create({
    //     openIn: 'page', //open in page
    //     openerEl: '#autocomplete-city', //link that opens autocomplete
    //     closeOnSelect: true, //go back after we select something
    //     source: function (query, render) {
    //         var results = [];
    //         if (query.length === 0) {
    //             render(results);
    //             return;
    //         }
    //         // Find matched items
    //         for (var i = 0; i < cities.length; i++) {
    //             if (cities[i].toLowerCase().indexOf(query.toLowerCase()) >= 0) results.push(cities[i]);
    //         }
    //         // Render items by passing array with result items
    //         render(results);
    //     },
    //     on: {
    //         change: function (value) {
    //             console.log(value);
    //             // Add item text value to item-after
    //             $$('#autocomplete-city').find('.item-after').text(value[0]);
    //             // Add item value to input value
    //             $$('#autocomplete-city').find('input').val(value[0]);
    //         },
    //     },
    // });

    var autocompleteDropdownAll = app.autocomplete.create({
        inputEl: '#autocomplete-city-dropdown-all',
        openIn: 'dropdown',
        source: function (query, render) {
            var results = [];
            // Find matched items
            for (var i = 0; i < cities.length; i++) {
                if (cities[i].toLowerCase().indexOf(query.toLowerCase()) >= 0) results.push(cities[i]);
            }
            // Render items by passing array with result items
            render(results);
        }
    });

});


$$(document).on('page:init', '.page[data-name="registration-3"]', function (e) {

    $$('.js-open-alert').on('click', function () {
        var dialog = app.dialog.create({
            title: 'Фото',
            text: '<a class="icon-close icon-close-top js-modal-close" onclick="app.dialog.close()"><i class="material-icons">clear</i></a>',

            buttons: [
                {
                    text: 'Удалить фото',
                    cssClass: 'color-red',
                },
                {
                    text: 'Новое фото',
                },
                {
                    text: 'Выбрать из библиотеки',
                },
                {
                    text: 'Отмена',
                    cssClass: 'color-black',
                },
            ],
            verticalButtons: true,
        }).open();
    });

    $$('.dialog-backdrop').on('click', function () {
        app.dialog.close()
    });

});





// var mainView = app.addView('.view-main', {
//     dynamicNavbar: true
// });


//
// // create searchbar
// // var searchbar = app.searchbar.create({
// //     el: '.searchbar',
// //     searchContainer: '.list',
// //     searchIn: '.item-title',
// //     on: {
// //         search(sb, query, previousQuery) {
// //             console.log(query, previousQuery);
// //         }
// //     }
// // });
//

//
//
// // Dom Events
// $$('.panel-left').on('panel:open', function () {
//     console.log('Panel left: open');
// });
// $$('.panel-left').on('panel:opened', function () {
//     console.log('Panel left: opened');
// });
//
//
// // App Events
// app.on('panelClose', function (panel) {
//     console.log('Panel ' + panel.side + ': close');
// });
// app.on('panelClosed', function (panel) {
//     console.log('Panel ' + panel.side + ': closed');
// });
//
//
//
// // Alert
// $$('.open-alert').on('click', function () {
//     app.dialog.alert('<b>Еще немного</b><br>Пожалуйста выберите ваш вид спорта, указав его уровень.');
// });
//
// $$('.btn-2bottom').click( function () {
//     scroll_end_of_chat();
// });
//

//temp
// $$('a[href="/club-item/"]').click();
// $$('a[href="/player-search/"]').click();
$$('a[href="/registration-2/"]').click();
// $$('a.panel-open[data-panel="left"]').click();
